

var toolbar_tickets = [
    {view: 'button', id: 'cmd_nuevo_ticket', label: 'Nuevo', type: 'iconButton',
        autowidth: true, icon: 'plus'},
    {view: 'button', id: 'cmd_ticket_to_invoice', label: 'Facturar',
        type: 'iconButton', autowidth: true, icon: 'file-code-o'},
    {},
    {view: 'button', id: 'cmd_ticket_report_pdf', label: 'Reporte',
        type: 'iconButton', autowidth: true, icon: 'file-pdf-o'},
    {view: 'button', id: 'cmd_ticket_report_xls', label: 'Reporte',
        type: 'iconButton', autowidth: true, icon: 'table'},
    {},
    {view: 'button', id: 'cmd_cancelar_ticket', label: 'Cancelar',
        type: 'iconButton', autowidth: true, icon: 'ban'},
]


var toolbar_tickets_filter = [
    {view: 'button', id: 'cmd_ticket_filter_today', label: 'Hoy', type: 'iconButton',
        autowidth: true, icon: 'filter'},
    {view: 'richselect', id: 'filter_year_ticket', label: 'Año',
        labelAlign: 'right', labelWidth: 50, width: 150, options: []},
    {view: 'richselect', id: 'filter_month_ticket', label: 'Mes',
        labelAlign: 'right', labelWidth: 50, width: 200, options: months},
    {view: 'daterangepicker', id: 'filter_dates_ticket', label: 'Fechas',
        labelAlign: 'right', width: 300},
]


var grid_tickets_cols = [
    {id: 'index', header: '#', adjust: 'data', css: 'right',
        footer: {content: 'countRows', colspan: 3, css: 'right'}},
    {id: "id", header:"ID", hidden:true},
    {id: "serie", header: ["Serie", {content: "selectFilter"}], adjust: "header",
        sort: "string"},
    {id: 'folio', header: ['Folio', {content: 'numberFilter'}], adjust: 'header',
        sort: 'int', css: 'right', footer: {text: 'Tickets', colspan: 3}},
    {id: "fecha", header: ["Fecha y Hora"],
        adjust: "data", sort: "date"},
    {id: "estatus", header: ["Estatus", {content: "selectFilter"}],
        adjust: "data", sort:"string"},
    {id: 'total', header: ['Total', {content: 'numberFilter'}], width: 150,
        sort: 'int', format: webix.i18n.priceFormat, css: 'right',
        footer: {content: 'summActive', css: 'right'}},
    {id: "cliente", header: ["Razón Social", {content: "selectFilter"}],
        fillspace:true, sort:"string", hidden: true},
    {id: 'pdf', header: 'PDF', adjust: 'data', template: get_icon('pdf')},
    {id: 'print', header: '', adjust: 'data', template: get_icon('print')},
]


var grid_tickets = {
    view: 'datatable',
    id: 'grid_tickets',
    select: 'row',
    scrollY: true,
    adjust: true,
    footer: true,
    resizeColumn: true,
    headermenu: true,
    columns: grid_tickets_cols,
    scheme:{
        $change:function(item){
            if (item.estatus == 'Cancelado'){
                item.$css = 'cancel'
            }
        }
    },
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    },
}


var rows_tickets_home = [
    {view: 'toolbar', elements: toolbar_tickets},
    {view: 'toolbar', elements: toolbar_tickets_filter},
    grid_tickets,
]


var ticket_suggest_products = {
    view: 'gridsuggest',
    id: 'gt_productos_found',
    name: 'gt_productos_found',
    body: {
        autoConfig: false,
        header: true,
        columns: [
            {id: 'id', hidden: true},
            {id: 'clave', header: 'Clave', adjust: 'data'},
            {id: 'descripcion', header: 'Descripción', width: 500},
            {id: 'unidad', header: 'Unidad', adjust: 'data'},
            {id: 'valor_unitario', header: 'Valor Unitario', adjust: 'data',
                format: webix.i18n.priceFormat}
        ],
        dataFeed:function(text){
            if (text.length > 2){
                this.load('/values/product?name=' + text)
            }else{
                this.hide()
            }
        }
    },
}


var tbody_buscar_producto = {rows: [
    {cols: [
        {view: 'search', id: 'tsearch_product_key', name: 'tsearch_product_key',
            label: 'por Clave', labelPosition:'top', maxWidth: 250,
            placeholder: 'Presiona ENTER para buscar'},
        {view: 'search', id: 'tsearch_product_name', name: 'tsearch_product_name',
            label: 'por Descripción', labelPosition:'top',
            suggest: ticket_suggest_products, placeholder: 'Captura al menos tres letras'},
    ]},
]}


var grid_tdetails_cols = [
    {id: "id_product", header:"ID", hidden: true},
    {id: 'delete', header: '', width: 30, css: 'delete'},
    {id: "clave", header:{text: 'Clave', css: 'center'}, width: 100,
        footer: {text: 'Artículos', css: 'right_footer3'}},
    {id: "clave_sat", hidden: true},
    {id: "descripcion", header:{text: 'Descripción', css: 'center'},
        fillspace: true, footer: {content: 'countRows', css: 'footer3'}},
    {id: "unidad", header:{text: 'Unidad', css: 'center'}, width: 100,
        editor: 'select', options: 'values/unidades'},
    {id: 'cantidad', header: {text: 'Cantidad', css: 'center'}, width: 100,
        format: webix.i18n.numberFormat, css: 'right', editor: 'text',
        footer: {content: 'summColumn', css: 'right_footer3'}},
    {id: "valor_unitario", header:{text: 'Valor Unitario', css: 'center'},
        width: 100, format: webix.i18n.priceFormat, css: 'right', editor: 'text',
        footer: {text: 'Total ', css: 'right_footer2'}},
    {id: 'descuento', header: {text: 'Descuento', css: 'center'}, hidden: true,
        width: 80, format: webix.i18n.priceFormat, css: 'right', editor: 'text'},
    {id: 'precio_final', hidden: true, header: 'precio_final', width: 80,
        format: webix.i18n.priceFormat, css: 'right'},
    {id: "importe", header:{text: 'Importe', css: 'center'}, width: 150,
        format: webix.i18n.priceFormat, css: 'right',
        footer: {content: 'summColumn', css: 'right_footer2'}},
]


var grid_tdetails = {
    view: 'datatable',
    id: 'grid_tdetails',
    select: 'row',
    adjust: true,
    autoheight: true,
    editable: true,
    footer: true,
    columns: grid_tdetails_cols,
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.delete = '-'
            })
        }
    },
    data: [],
}


var body_ticket_informacion = {rows: [
    {view: 'richselect', id: 'lst_ticket_forma_pago', name: 'forma_pago',
        label: 'Forma de Pago', labelPosition: 'top', required: true,
        options: []},
],}


var toolbar_new_ticket = {
    view: 'toolbar', elements: [
        {view: 'button', id: 'cmd_generar_ticket', label: 'Generar',
            icon: 'ticket', type: 'iconButton', autowidth: true,
            align: 'center', hotkey: 'Ctrl+g'}, {},
        {view: 'button', id: 'cmd_ticket_notes', label: 'Notas',
            autowidth: true, type: 'iconButton', icon: 'commenting-o'}, {},
        {view: 'button', id: 'cmd_cerrar_ticket', label: 'Cerrar',
            type: 'danger', autowidth: true, align: 'center'}
]}


var grid_ticket_total_up = {
    view: 'datatable',
    id: 'grid_ticket_total_up',
    adjust: true,
    autoheight: true,
    width: 250,
    header: false,
    footer: true,
    hidden: true,
    rowHeight: 0,
    columns: [
        {id: 'id', hidden: true},
        {id: 'title', width: 100,
            footer: {text: 'Total ', css: 'right_footer2'}},
        {id: 'total', fillspace: true, format: webix.i18n.priceFormat,
            footer: {content: 'summColumn', css: 'right_footer2'}},
    ],
    data: [{id:0, title: '', total: 0.00}]
}


var cells_new_ticket = [
    {minHeight: 10, maxHeight: 10},
    toolbar_new_ticket,
    {minHeight: 10, maxHeight: 10},
    {cols: [{rows: [
        {view: 'fieldset', label: 'Buscar Producto', body: tbody_buscar_producto},
        ]},
        {maxWidth: 10},
        {maxWidth: 300, rows: [
            {view: 'fieldset', label: 'Información', body: body_ticket_informacion},
        ]},
    ]},
    {cols: [
        {view: 'label', label: 'Detalle', height: 30, align: 'left'},
        {},
        grid_ticket_total_up,
    ]},
    grid_tdetails,
]


var toolbar_ticket_invoice = {view: 'toolbar', elements: [{},
    {view: 'checkbox', id: 'chk_is_invoice_day', labelWidth: 0, width: 150,
        labelRight: 'Es factura del día'}, {},
    {view: 'button', id: 'cmd_close_ticket_invoice', label: 'Cerrar',
        type: 'danger', autowidth: true, align: 'center'}
]}


var ticket_suggest_partners = {
    view: 'gridsuggest',
    id: 'grid_ticket_clients_found',
    name: 'grid_ticket_clients_found',
    body: {
        autoConfig: false,
        header: false,
        columns: [
            {id: 'id', hidden: true},
            {id: 'nombre', adjust: 'data'},
            {id: 'rfc', adjust: 'data'},
            {id: 'forma_pago', hidden: true},
            {id: 'uso_cfdi', hidden: true},
        ],
        dataFeed:function(text){
            if (text.length > 2){
                this.load('/values/client?name=' + text)
            }else{
                this.hide()
            }
        }
    }
}


var ticket_search_client = {cols: [{rows: [
    {view: 'fieldset', id: 'fs_ticket_search_client', label: 'Buscar Cliente', body: {rows: [
        {cols: [
            {view: 'search', id: 'tsearch_client_key', name: 'tsearch_client_key',
                label: 'por Clave', labelPosition: 'top', maxWidth: 250,
                placeholder:'Presiona ENTER para buscar'},
            {view: 'search', id: 'tsearch_client_name',
                name: 'tsearch_client_name', label: 'por Nombre o RFC',
                labelPosition: 'top', suggest: ticket_suggest_partners,
                placeholder: 'Captura al menos tres letras'},
        ]},
        {cols: [
            {view: 'label', id: 'lbl_tclient_title', autowidth:true,
                name: "lbl_tclient_title", label: 'Seleccionado: ' },
            {view: 'label', id: 'lbl_tclient', name: 'lbl_tclient',
                label: 'Ninguno'},
        ]}
    ]}},
]},]}


var grid_tickets_active_cols = [
    {id: 'index', header: '#', adjust: 'data', css: 'right'},
    {id: "id", header:"ID", hidden:true},
    {id: "serie", header: ["Serie", {content: "selectFilter"}], adjust: "data",
        sort: "string"},
    {id: 'folio', header: ['Folio', {content: 'numberFilter'}], adjust: 'header',
        sort: 'int', css: 'right', footer: {content: 'countRows', css: 'right'}},
    {id: "fecha", header: ["Fecha y Hora"], adjust: "data", sort: "string",
        footer: 'Tickets'},
    {id: 'total', header: 'Total', width: 150,sort: 'int', css: 'right',
        format: webix.i18n.priceFormat, footer: {content: 'summColumn',
            css: 'right'}},
]


var grid_tickets_active = {
    view: 'datatable',
    id: 'grid_tickets_active',
    select: 'row',
    adjust: true,
    footer: true,
    drag: true,
    resizeColumn: true,
    headermenu: true,
    columns: grid_tickets_active_cols,
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    },
}


var grid_tickets_invoice_cols = [
    {id: 'index', header: '#', adjust: 'data', css: 'right'},
    {id: "id", header:"ID", hidden:true},
    {id: "serie", header: ["Serie", {content: "selectFilter"}], adjust: "data",
        sort: "string"},
    {id: 'folio', header: ['Folio', {content: 'numberFilter'}], adjust: 'header',
        sort: 'int', css: 'right', footer: {content: 'countRows', css: 'right'}},
    {id: "fecha", header: ["Fecha y Hora"], adjust: "data", sort: "string",
        footer: 'Tickets'},
    {id: 'total', header: 'Total', width: 150,sort: 'int', css: 'right',
        format: webix.i18n.priceFormat, footer: {content: 'summColumn',
            css: 'right'}},
]


var grid_tickets_invoice = {
    view: 'datatable',
    id: 'grid_tickets_invoice',
    select: 'row',
    adjust: true,
    footer: true,
    drag: true,
    resizeColumn: true,
    headermenu: true,
    columns: grid_tickets_invoice_cols,
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    },
}


var controls_ticket_to_invoice = [
    {minHeight: 10, maxHeight: 10},
    toolbar_ticket_invoice,
    {minHeight: 10, maxHeight: 10},
    ticket_search_client,
    {minHeight: 5, maxHeight: 5},
    {cols:[
        {rows: [{view: 'label', label: 'Tickets sin facturar', height: 30,
            align: 'left'},
            grid_tickets_active]},
        {rows:[{},
            {view: 'button', id: 'cmd_move_tickets_right', label: '->', autowidth: true},
            {view: 'button', id: 'cmd_move_tickets_left', label: '<-', autowidth: true},
            {}]},
        {rows: [{view: 'label', label: 'Tickets a facturar', height: 30,
            align: 'left'},
            grid_tickets_invoice]},
    ]},
    {minHeight: 20, maxHeight: 20},
    {margin: 20, cols: [{},
        {view: 'button', id: 'cmd_new_invoice_from_ticket', label: 'Facturar',
            icon: 'ticket', type: 'iconButton', autowidth: true, align: 'center'},
        {}]
    },
]


var controls_new_ticket = [
    {
        view: 'tabview',
        id: 'tv_new_ticket',
        animate: true,
        cells: [
            {id: 'Generar', rows: cells_new_ticket},
        ]
    },
]


var controls_ticket_invoice = [
    {
        view: 'tabview',
        id: 'tv_ticket_invoice',
        animate: true,
        cells: [
            {id: 'Facturar Tickets', rows: controls_ticket_to_invoice},
        ]
    },
]


var form_new_ticket = {
    type: 'space',
    responsive: true,
    cols: [{
        view: 'form',
        id: 'form_new_ticket',
        complexData: true,
        scroll: true,
        elements: controls_new_ticket,
    }],
}


var form_ticket_invoice = {
    type: 'space',
    responsive: true,
    cols: [{
        view: 'form',
        id: 'form_ticket_invoice',
        complexData: true,
        scroll: true,
        elements: controls_ticket_invoice,
    }],
}


var multi_tickets = {
    id: 'multi_tickets',
    view: 'multiview',
    animate: true,
    cells:[
        {id: 'tickets_home', rows: rows_tickets_home},
        {id: 'tickets_new', rows:[form_new_ticket]},
        {id: 'tickets_invoice', rows:[form_ticket_invoice]}
    ],
}


var app_tickets = {
    id: 'app_tickets',
    rows:[
        {view: 'template', id: 'th_ticckets', type: 'header',
            template: 'Punto de venta - Tickets'},
        multi_tickets
    ],
}


var body_ticket_notes = {rows: [
    {minHeight: 5, maxHeight: 5},
    {view: 'textarea', id: 'ticket_notes', name: 'ticket_notes', height: 300,
        placeholder: 'Captura las notas'},
    {minHeight: 5, maxHeight: 5},
    {cols: [{}, {view: 'button', id: 'cmd_ticket_save_note', autowidth: true,
        label: 'Guardar y Cerrar', type: 'iconButton', hotkey: 'Ctrl+enter'},
        {}]},
    {minHeight: 5, maxHeight: 5},
]}


var win_ticket_notes = {
    init: function(){
        webix.ui({
            view: 'window',
            id: 'win_ticket_notes',
            height: 400,
            width: 500,
            modal: true,
            position: 'center',
            head: 'Notas',
            body: body_ticket_notes,
        })
        $$('cmd_ticket_save_note').attachEvent('onItemClick', cmd_ticket_save_note_click)
    }
}